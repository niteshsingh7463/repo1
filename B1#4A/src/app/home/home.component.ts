import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { baseUrl } from './../myData';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  allData: any;
  constructor(private route: ActivatedRoute, private authService: AuthService, private router: Router, private httpClient: HttpClient) { }
  myUrl = baseUrl;
  simpleForm: FormGroup = null;
  sortVar = null;
  fuelVar = null;
  typeVar = null;
  ngOnInit() {
    this.route.queryParamMap.subscribe(resp => {
      this.authService.getData(this.myUrl + this.router.url).subscribe(data => { this.allData = data; console.log(this.router.url) })
    })
    this.authService.getData(this.myUrl + this.router.url).subscribe(data => { this.allData = data; console.log(this.router.url) })
    this.simpleForm = new FormGroup({
      'minprice': new FormControl(null),
      'maxprice': new FormControl(null)
    })
  }
  editFun(id) {
    this.router.navigate(['/car/' + id]);
  }
  delFun(id) {
    this.httpClient.delete(this.myUrl + '/cars/' + id).subscribe(resp => { console.log(resp); });
    this.authService.getData(this.myUrl + '/cars').subscribe(data => { this.allData = data; })

  }
  sortChange(v1) {
    this.sortVar = v1;
    this.onChangeOpt();
  }

  fuelChange(v1) {
    this.fuelVar = v1;
    this.onChangeOpt();
  }
  typeChange(v1) {
    this.typeVar = v1;
    this.onChangeOpt();
  }

  onChangeOpt() {
    console.log('working', this.simpleForm.get('minprice').value)
    this.router.navigate(['/cars'], { queryParams: { sort: this.sortVar, type: this.typeVar, fuel: this.fuelVar, maxprice: this.simpleForm.get('maxprice').value, minprice: this.simpleForm.get('minprice').value } })
  }
}
