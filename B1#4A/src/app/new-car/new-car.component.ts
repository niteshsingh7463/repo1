import { HttpClient } from '@angular/common/http';
import { baseUrl } from './../myData';
import { AuthService } from './../auth.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-new-car',
  templateUrl: './new-car.component.html',
  styleUrls: ['./new-car.component.css']
})
export class NewCarComponent implements OnInit {
  simpleForm: FormGroup = null;
  constructor(private httpClient: HttpClient, private authService: AuthService, private router: Router, private route: ActivatedRoute) { }
  carmaster: any;
  colorOpt;
  myUrl = baseUrl;
  editBool = false;
  editId = null;
  cars: any = [{ id: '', price: '', year: '', kms: '', model: "", color: "" },];


  ngOnInit() {

    this.route.paramMap.subscribe(resp => {
      if (resp.get('id')) {
        this.editId = resp.get('id'); console.log(this.editId); this.editBool = true;
      }
    });

    this.authService.getData(this.myUrl + '/carmaster').subscribe(data => {
      this.carmaster = data;
    });


    this.authService.getData(this.myUrl + '/cars').subscribe(data => {
      this.cars = data; if (this.editBool) { this.edit1(this.editId) }
    });

    this.simpleForm = new FormGroup({
      'carid': new FormControl({ value: '', disabled: this.editBool }),
      'price': new FormControl(''),
      'kms': new FormControl(''),
      'year': new FormControl(''),
      'model': new FormControl(''),
      'color': new FormControl('')
    });

  }

  createColorOpt() {
    let model1 = this.simpleForm.get('model').value;
    this.colorOpt = this.carmaster.find(n1 => n1.model == model1).colors;

  }
  edit1(editId) {
    // console.log(this.cars);
    let data1 = this.cars.find(n1 => n1.id == editId);
    this.simpleForm.get('carid').setValue(data1.id);
    this.simpleForm.get('price').setValue(data1.price);
    this.simpleForm.get('year').setValue(data1.year);
    this.simpleForm.get('kms').setValue(data1.kms);
    this.simpleForm.get('model').setValue(data1.model);
    this.createColorOpt();
    this.simpleForm.get('color').setValue(data1.color);
    this.editBool = true;
  }
  update(data) { this.httpClient.put(this.myUrl + '/cars/' + this.editId, data).subscribe(resp => { alert(this.editId + ' updated'); console.log('put resp::', resp) }) }
  post(data) {
    this.authService.postData(this.myUrl + '/cars', data).subscribe(resp => { alert(data.id + ' inserted'); console.log('post resp::', resp) });
  }
  onSubmit() {
    let id1 = this.simpleForm.get('carid').value;
    let price1 = this.simpleForm.get('price').value;
    let year1 = this.simpleForm.get('year').value;
    let kms1 = this.simpleForm.get('kms').value;
    let model1 = this.simpleForm.get('model').value;
    let color1 = this.simpleForm.get('color').value;
    let data2 = { id: id1, price: price1, year: year1, kms: kms1, model: model1, color: color1 }
    if (this.editBool) {
      this.update(data2);
    } else { this.post(data2) }

    this.editBool = false;

    this.router.navigate(['/cars']);
  }



}
