import { NewCarComponent } from './new-car/new-car.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'cars',
  component: HomeComponent
},

{
  path: 'car',
  component: NewCarComponent,
},
{
  path: 'car/:id',
  component: NewCarComponent,
  pathMatch: 'full'
},
{
  path: '',
  redirectTo: '/cars',
  pathMatch: 'full'
},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
