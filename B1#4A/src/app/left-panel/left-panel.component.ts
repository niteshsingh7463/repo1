import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.css']
})
export class LeftPanelComponent implements OnInit {
  @Output() sort3 = new EventEmitter();
  @Output() fuel3 = new EventEmitter();
  @Output() type3 = new EventEmitter();
  sortOpt = { options: ["kms", "price", "year"], isSelected: null };
  fuelOpt = { options: ['Diesel', 'Petrol'], isSelected: null };
  typeOpt = { options: ["Hatchback", "Sedan"], isSelected: null };
  constructor() { }

  ngOnInit() {
  }
  changeOpt() {
    this.sort3.emit(this.sortOpt.isSelected);
    this.fuel3.emit(this.fuelOpt.isSelected);
    this.type3.emit(this.typeOpt.isSelected);

  }
}
