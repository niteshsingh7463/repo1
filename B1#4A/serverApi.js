const { get } = require("core-js/fn/dict");
var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    req.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With,Content-Type,Accept"
    );
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    next();
});
const port = 2410;
app.listen(port, () => console.log("Listening on port : ", port));
let carMaster = require("./carData.js").carMaster;
let carsList = require("./carData.js").cars;

// ===========================Get====================================

app.get("/cars", function(req, res) {
    let minprice1 = req.query.minprice;
    let maxprice1 = req.query.maxprice;
    let fuel1 = req.query.fuel;
    let type1 = req.query.type;
    let sort1 = req.query.sort;
    console.log("In get request for stars: ", sort1);
    let arr = carsList;
    if (minprice1) {
        arr = arr.filter((n1) => n1.price >= +minprice1);
    }

    if (maxprice1) {
        arr = arr.filter((n1) => n1.price <= +maxprice1);
    }

    if (fuel1) {
        let allFuel = carMaster.filter((n1) => n1.fuel == fuel1);
        arr = arr.filter((n1) => allFuel.find((n) => n.model == n1.model));
    }

    if (type1) {
        let allModel = carMaster.filter((n1) => n1.type == type1);
        arr = arr.filter((n1) => allModel.find((n) => n.model == n1.model));
    }

    if (sort1 == "kms") {
        arr.sort((n1, n2) => n1.kms - n2.kms);
    }

    if (sort1 == "price") {
        arr.sort((n1, n2) => n1.price - n2.price);
    }

    if (sort1 == "year") {
        arr.sort((n1, n2) => n1.year - n2.year);
    }
    res.send(arr);
});
// -------------------------------------------------------------
app.get("/cars/:id", function(req, res) {
    let id = req.params.id;
    let car = carsList.find((n1) => n1.id == id);
    res.send(car);
});

// -------------------------------------------------------------
app.get("/carmaster", function(req, res) {
    res.send(carMaster);
});
// ===========================Post====================================

app.post("/cars", function(req, res) {
    let body = req.body;
    carsList.push(body);
    res.send(body);
});

// ===========================Put====================================

app.put("/cars/:id", function(req, res) {
    let id = req.params.id;
    let body = req.body;
    let index = carsList.findIndex((n1) => n1.id == id);
    carsList.splice(index, 1, body);
    res.send(body);
});

// ===========================Delete====================================

app.delete("/cars/:id", function(req, res) {
    let id = req.params.id;
    let index = carsList.findIndex((n1) => n1.id == id);
    let data = carsList.splice(index, 1);
    res.send(data);
});
