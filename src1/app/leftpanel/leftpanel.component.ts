import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-leftpanel',
  templateUrl: './leftpanel.component.html',
  styleUrls: ['./leftpanel.component.css']
})
export class LeftpanelComponent implements OnInit {
  @Input() allData;
  @Input() bestSellerOptions;
  @Input() languageOptions;
  @Output() best = new EventEmitter();
  @Output() optChange = new EventEmitter();

  constructor() { }

  ngOnInit() {

  }
  bestSellerChange() {
    this.best.emit();
  }
  changeOpt() {
    this.optChange.emit();
  }
}
