
import { NetService } from './../net.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  allData: any = {
    "data": [
      {
        "bookid": "1",
        "name": "The Lord of the Rings",
        "author": "J. R. R. Tolkien",
        "year": "1954",
        "genre": "Fiction",
        "newarrival": "Yes",
        "bestseller": "Yes",
        "language": "English",
        "publisher": "Pengiun",
        "price": "740",
        "blurb": "Blurb::The Lord of the Rings:J. R. R. Tolkien:1954",
        "description": "Description::The Lord of the Rings:J. R. R. Tolkien:1954",
        "avgrating": "9",
        "review": "Review::The Lord of the Rings:J. R. R. Tolkien:1954"
      }]
    ,
    "pageInfo": {
      "pageNumber": 1,
      "numberOfPages": 15,
      "numOfItems": 10,
      "totalItemCount": 147
    },
    "refineOptions": {
      "bestseller": [
        {
          "totalNum": 73,
          "refineValue": "Yes"
        },
        {
          "totalNum": 74,
          "refineValue": "No"
        }
      ],
      "language": [
        {
          "totalNum": 16,
          "refineValue": "French"
        },
        {
          "totalNum": 83,
          "refineValue": "English"
        },
        {
          "totalNum": 21,
          "refineValue": "Latin"
        },
        {
          "totalNum": 27,
          "refineValue": "Other"
        }
      ],
      "publisher": [
        {
          "totalNum": 45,
          "refineValue": "Pengiun"
        },
        {
          "totalNum": 26,
          "refineValue": "HarperCollins"
        },
        {
          "totalNum": 18,
          "refineValue": "Knopf"
        },
        {
          "totalNum": 58,
          "refineValue": "Other"
        }
      ]
    }

  };
  baseUrl = 'http://localhost:2410/booksapp/books/';
  currDisplayarr = this.allData.data;
  category = null;
  newarrival = null;
  page = null;
  start = null;
  end = null;

  // ===========================Variable Declaration=========================
  bestSellerOptions = null;
  languageOptions = null;
  bestseller = null;
  language = null;

  //=============================Variable Declaration End====================

  constructor(private netservice: NetService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => { this.makeNull(); this.constructOpt(); this.category = params.get('category'); this.getD(); });
    // ---------------------------queryParam-------------------------------------------
    this.route.queryParamMap.subscribe(params => {
      this.constructOpt();
      this.newarrival = params.get('newarrival');
      this.page = params.get('page');
      this.bestseller = params.get('bestseller');
      this.language = params.get('language');
      this.assignOpt();
      this.getD();

    });
    this.constructOpt();
  }
  assignOpt() {
    if (this.bestseller) {
      this.bestseller = this.bestseller.split(',');
      for (let i = 0; i < this.bestseller.length; i++) {
        this.bestSellerOptions.options.find(n1 => n1.refineValue == this.bestseller[i]).isSelected = true;
      }
    }
    if (this.language) {
      this.language = this.language.split(',');
      for (let i = 0; i < this.language.length; i++) {
        this.languageOptions.options.find(n1 => n1.refineValue == this.language[i]).isSelected = true;
      }
    }
    this.reConstructOpt();
  }


  constructOpt() {
    this.bestSellerOptionsFun();
    this.langaugeOptiosFun();
  }
  makeNull() {
    this.category = null;
    this.newarrival = null;
    this.page = null;
    this.baseUrl = 'http://localhost:2410/booksapp/books/';
  }
  getD() {
    // console.log(this.router.url);
    this.netservice.getData('http://localhost:2410/booksapp' + this.router.url).subscribe(resp => {
      this.allData = resp; this.currDisplayarr = this.allData.data; this.makeStartEnd();


      // ========================================================
      let temp1 = this.bestSellerOptions.options;
      let temp2 = this.languageOptions.options;
      for (let i = 0; i < temp1.length; i++) {
        temp1[i].totalNum = this.allData.refineOptions.bestseller.find(n1 => n1.refineValue == temp1[i].refineValue).totalNum;

      }

      for (let i = 0; i < temp2.length; i++) {
        temp2[i].totalNum = this.allData.refineOptions.language.find(n1 => n1.refineValue == temp2[i].refineValue).totalNum;
      }
      // console.log(this.bestSellerOptions);
      // ================================================

    });
  }

  makeStartEnd() {
    this.start = 1 + (this.allData.pageInfo.pageNumber - 1) * 10;
    this.end = this.allData.pageInfo.pageNumber * 10;
    if (this.end > this.allData.pageInfo.totalItemCount)
      this.end = this.allData.pageInfo.totalItemCount;
  }

  next() {
    let path = '/books/'
    if (this.category)
      path = path + this.category
    this.router.navigate([path], { queryParams: { newarrival: this.newarrival, language: this.languageOptions.selected, bestseller: this.bestSellerOptions.selected, page: this.allData.pageInfo.pageNumber + 1 } })
  }


  previous() {
    let path = '/books/'
    if (this.category)
      path = path + this.category
    this.router.navigate([path], { queryParams: { newarrival: this.newarrival, language: this.languageOptions.selected, bestseller: this.bestSellerOptions.selected, page: this.allData.pageInfo.pageNumber - 1 } })
  }

  // ====================================Left panel===================

  reConstructOpt() {
    let bst = this.bestSellerOptions.options.filter(n1 => n1.isSelected == true);
    let lan = this.languageOptions.options.filter(n1 => n1.isSelected == true);

    if (bst.length != 0) {

      this.bestSellerOptions.options = bst;
      let bststr = bst.map(n1 => n1.refineValue).join(',');
      this.bestSellerOptions.selected = bststr;

    }

    // ---------------------------------------
    if (lan.length != 0) {
      this.languageOptions.options = lan;
      let lanstr = lan.map(n1 => n1.refineValue).join(',');

      this.languageOptions.selected = lanstr;

    }

  }

  bestSellerChange() {
    // console.log(this.bestSellerOptions);
    this.reConstructOpt();
    let path = '/books/'
    if (this.category)
      path = path + this.category
    this.router.navigate([path], { queryParams: { newarrival: this.newarrival, language: this.languageOptions.selected, bestseller: this.bestSellerOptions.selected } })

  }
  changeOpt() {
    let bst = this.bestSellerOptions.options.filter(n1 => n1.isSelected == true);
    let lan = this.languageOptions.options.filter(n1 => n1.isSelected == true);
    if (bst.length == 0 && lan.length == 0) {
      this.bestSellerOptionsFun();
      this.langaugeOptiosFun();
      let path = '/books/'
      if (this.category)
        path = path + this.category
      this.router.navigate([path], { queryParams: { newarrival: this.newarrival, language: this.languageOptions.selected, bestseller: this.bestSellerOptions.selected } })

    }
  }
  bestSellerOptionsFun() {
    this.bestSellerOptions = {
      options: this.createOpt(this.allData.refineOptions.bestseller),
      display: 'Bestseller',
      selected: null
    }
  }


  langaugeOptiosFun() {
    this.languageOptions = {
      options: this.createOpt(this.allData.refineOptions.language),
      display: 'Language',
      selected: null
    }
  }


  createOpt(n) {
    let opt = [];
    for (let i = 0; i < n.length; i++) { opt.push({ totalNum: n[i].totalNum, refineValue: n[i].refineValue, isSelected: false }) }
    return opt;
  }
  // =============================Left Panel End=====================================
}
