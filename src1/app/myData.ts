class DataType {
  "bookid": string;
  "name": string;
  "author": string;
  "year": string;
  "genre": string;
  "newarrival": string;
  "bestseller": string;
  "language": string;
  "publisher": string;
  "price": string;
  "blurb": string;
  "description": string;
  "avgrating": string;
  "review": string;

}

class Pageinfo {
  "pageNumber": number;
  "numberOfPages": number;
  "numOfItems": number;
  "totalItemCount": number

}

class RefineOpt {
  bestseller: { totalNum: number; refineValue: string }[];
  language: { totalNum: number; refineValue: string }[];
  publisher: { totalNum: number; refineValue: string }[];

}


export class TypeData {

  "data": DataType[];
  "pageInfo": Pageinfo;

  "refineOptions": RefineOpt

}
