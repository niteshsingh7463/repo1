import { NetServiceService } from './../net-service.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.css']
})
export class LeftPanelComponent implements OnInit {
  @Output() radioChange = new EventEmitter();
  constructor(private netservice: NetServiceService) { }
  languageVar = null;
  languageArr = [{ name: 'English', value: 'en' }, { name: 'French', value: 'fr' }, { name: 'Hindi', value: 'hi' }];

  filterArr = [{ name: 'Full Volume', value: 'full' }, { name: 'Free Google e-books', value: 'free-ebooks' }, { name: 'Paid Google e-books', value: 'paid-ebooks' }];
  filterVar = null;
  ngOnInit() {
    this.languageVar = null;
    this.filterVar = null;
  }
  btnChange() {
    let p1 = [this.languageVar, this.filterVar];

    this.radioChange.emit(p1);

  }
}
